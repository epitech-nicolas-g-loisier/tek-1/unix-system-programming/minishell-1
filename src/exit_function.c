/*
** EPITECH PROJECT, 2018
** Minishell1
** File description:
** exit function
*/

#include <stdlib.h>
#include <unistd.h>
#include "my.h"

int	exit_function(char *value)
{
	int ret = 0;
	
	if (value == NULL) {
		write(1, "exit\n", 5);
		exit(0);
	}
	else {
		if (my_str_isnum(value) == 0){
			write(2, "exit: Badly formed number.\n", 27);
			return (84);
		}
		else {
			write(1, "exit\n", 5);
			ret = my_getnbr(value);
			exit(ret);
		}
	}	
}
