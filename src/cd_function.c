/*
** EPITECH PROJECT, 2018
** Minishell1
** File description:
** cd function
*/

#include <unistd.h>
#include <stdlib.h>
#include <linux/limits.h>
#include "my.h"
#include "minishell.h"

int	print_cd_error(char *str)
{
	write(2, "cd: no such file or directory: ", 31);
	write(2, str, my_strlen(str));
	write(2, "\n", 1);
	return (84);
}

char	*go_to_home(char **env, char *old_dir)
{
	int idx = 0;
	char *dir = malloc(sizeof(char) * PATH_MAX + 1);

	dir = getcwd(dir, PATH_MAX);
	while (env[idx] != NULL) {
		if (find_name(env[idx], "HOME") == 0) {
			break;
		}
		idx++;
	}
	if (env[idx] != NULL){
		chdir(my_strdup(&env[idx][5]));
		return (dir);
	} else {
		free(dir);
		return (old_dir);
	}
}

int	cd_function(char **command, char **env)
{
	int check = 0;
	static char *old_dir = NULL;
	char *tmp = malloc(sizeof(char) * PATH_MAX + 1);;

	if (command[1] == NULL || my_strcmp("~", command[1]) == 0){
		old_dir = go_to_home(env, old_dir);
	} else if (my_strcmp("-", command[1]) == 0) {
		tmp = getcwd(tmp, PATH_MAX);
		chdir(old_dir);
		old_dir = my_strdup(tmp);
	} else {
		old_dir = malloc(sizeof(char) * PATH_MAX + 1);
		old_dir = getcwd(old_dir, PATH_MAX);
		check = chdir(command[1]);
	}
	if (check == -1) {
		return (print_cd_error(command[1]));
	}
	free(tmp);
	return 0;
}
