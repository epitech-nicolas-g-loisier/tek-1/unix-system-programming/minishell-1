/*
** EPITECH PROJECT, 2018
** Minishell1
** File description:
** setenv function
*/

#include <unistd.h>
#include <stdlib.h>
#include "my.h"
#include "minishell.h"

char	*get_name(char *name, char *value)
{
	int idx = 0;
	int idx2 = 0;
	int length = my_strlen(name) + my_strlen(value);
	char *new_name = malloc(sizeof(char) * (length + 2));

	while (name[idx] != '\0') {
		if (name[idx] == '=')
			return (NULL);
		new_name[idx] = name[idx];
		idx++;
	}
	new_name[idx] = '=';
	idx++;
	while (value[idx2] != '\0') {
		new_name[idx] = value[idx2];
		idx++;
		idx2++;
	}
	new_name[idx] = '\0';
	return (new_name);
}

int	check_name(char *line, char *name)
{
	int idx = 0;

	while (name[idx] != '\0') {
		if (line[idx] != name[idx])
			return (1);
		idx++;
	}
	if (name[idx] == '\0' && line[idx] == '=') {
		return (0);
	} else {
		return (1);
	}
}

char	**get_new_env(int size, char **env, char *name)
{
	int line_idx = 0;
	char **new_env = malloc(sizeof(char*) * size + 2);

	while (env[line_idx]) {
		new_env[line_idx] = my_strdup(env[line_idx]);
		line_idx++;
	}
	new_env[line_idx] = name;
	line_idx++;
	new_env[line_idx] = NULL;
	return (new_env);
}

char	**setenv_function(char **command, char **env)
{
	char **new_env;
	char *name;
	int idx = 0;

	if (command[1] == NULL || command[2] == NULL)
		return (env);
	name = get_name(command[1], command[2]);
	if (name == NULL)
		return (env);
	while (env[idx] && my_getnbr(command[3]) != 0) {
		if (check_name(env[idx], command[1]) == 0) {
			env[idx] = name;
			return (env);
		}
		idx++;
	}
	new_env = get_new_env(idx, env, name);
	return (new_env);
}
