/*
** EPITECH PROJECT, 2018
** Minishell1
** File description:
** recover path of executable
*/

#include <unistd.h>
#include <stdlib.h>
#include "my.h"
#include "minishell.h"

char	*get_path_line(char **env)
{
	int idx = 0;

	while (env[idx] != NULL){
		if (env[idx][0] == 'P' && env[idx][1] == 'A' &&
			env[idx][2] == 'T' && env[idx][3] == 'H')
			return (my_strdup(env[idx]));
		else
			idx++;
	}
	return (NULL);
}

char	*get_path(char *str, int idx)
{
	int count = idx;
	int counter = 0;
	char *path;

	while (str[count] != ':' && str[count] != '\0'){
		count++;
	}
	path = malloc(sizeof(char) * (count - idx + 2));
	if (path == NULL)
		exit(84);
	while (idx < count){
		path[counter] = str[idx];
		idx++;
		counter++;
	}
	if (path[counter - 1] != '/'){
		path[counter] = '/';
		counter++;
	}
	path[counter] = '\0';
	return (path);
}

int	count_path(char *path)
{
	int count = 0;
	int idx = 0;

	while (path[idx] != '\0') {
		if (path[idx] == ':'){
			count++;
		}
		idx++;
	}
	return (count);
}

char	**path_parser(char *path)
{
	int count = 0;
	int idx = 0;
	char **tab;

	if (path == NULL)
		return NULL;
	tab = malloc(sizeof(char*) * (count_path(path) + 2));
	while (path[idx] != '\0') {
		if (path[idx] == '=' || path[idx] == ':') {
			tab[count] = get_path(path, (idx + 1));
			idx += my_strlen(tab[count]) - 1;
			count++;
		}
		idx++;
	}
	tab[count] = NULL;
	return (tab);
}
