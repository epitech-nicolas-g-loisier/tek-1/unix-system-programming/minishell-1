/*
** EPITECH PROJECT, 2018
** Minishell1
** File description:
** function launch with the binary
*/

#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "my.h"
#include "minishell.h"

int	check_signal(int status)
{
	if (WIFSIGNALED(status)) {
		if (WTERMSIG(status) == SIGSEGV) {
			write(2, "Segmentation fault\n", 19);
			return (84);
		}
	}
	return (0);
}

int	check_access(char *command)
{
	if (access(command, X_OK) != 0) {
		write(2, command, my_strlen(command));
		write(2, ": Command not found.\n", 22);
		return (0);
	}
	return (1);
}

int	launch_binary(char **command, char **env)
{
	pid_t child_pid;
	pid_t wpid;
	int status = 50;

	if (check_access(command[0]) == 0) {
		return (84);
	}
	child_pid = fork();
	if (child_pid == -1) {
		return (84);
	}
	else if (child_pid == 0) {
		execve(command[0], command, env);
	} else {
		wpid = waitpid(child_pid, &status, WUNTRACED);
		if (wpid == -1) {
			return (84);
		}
	}
	return (check_signal(status));
}

int	find_prog(char **command, char **env)
{
	char *path = get_path_line(env);
	char **path_tab = path_parser(path);
	char *prog = NULL;
	int idx = 0;
	int ret = 0;

	if (path == NULL)
		return (84);
	while (path_tab[idx] != NULL) {
		prog = my_strcat(path_tab[idx], command[0]);
		if (access(prog, X_OK) == 0) {
			command[0] = prog;
			ret = launch_binary(command, env);
			return (ret);
		}
		idx++;
	}
	write(2, command[0], my_strlen(command[0]));
	write(2, ": Command not found.\n", 22);
	return (84);
}

int	other_function(char **command, char **env)
{
	int idx = 0;
	int ret = 0;

	while (command[0][idx] != '\0') {
		if (command[0][idx] == '/') {
			ret = launch_binary(command, env);
			return (ret);
		}
		idx++;
	}
	ret = find_prog(command, env);
	return (ret);
}
