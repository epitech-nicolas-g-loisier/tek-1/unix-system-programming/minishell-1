/*
** EPITECH PROJECT, 2018
** Minishell 1
** File description:
** UNIX command interpreter
*/

#include <unistd.h>
#include <stdlib.h>
#include <linux/limits.h>
#include "my.h"
#include "get_next_line.h"
#include "minishell.h"

int	spe_env_function(char **command, char **env, int idx)
{
	if (idx == 3) {
		env = setenv_function(command, env);
	} else if (idx == 4){
		env = unsetenv_function(command, env);
	}
	return (0);
}

char	**choose_command(char **command, char **env)
{
	static int ret = 0;
	char *function[6] = {"cd", "env", "exit", "setenv", "unsetenv", NULL};
	int (*funct_tab[6])(char **, char **);
	int idx = 0;

	funct_tab[0] = *cd_function;
	funct_tab[1] = *env_function;
	funct_tab[5] = *other_function;
	while (function[idx]) {
		if (my_strcmp(function[idx], command[0]) == 0)
			break;
		idx++;
	}
	if (idx < 2 || idx == 5) {
		ret = funct_tab[idx](command, env);
	} else if (idx == 2) {
		exit_function(command[1]);
	} else {
		ret = spe_env_function(command, env, idx);
	}
	return (env);
}

void	launch_minishell(char **env)
{
	char *buffer = NULL;
	char *name = malloc(sizeof(char) * PATH_MAX + 1);
	char **command;

	while (env[0]){
		name = getcwd(name, PATH_MAX);
		write(1, name, my_strlen(name));
		write(1, " > ", 3);
		buffer = get_next_line(0);
		command = command_parser(buffer);
		if (my_strcmp(buffer, "") != 0)
			env = choose_command(command, &env[0]);
	}
}

void	display_usage(void)
{
	write(1, "mysh: UNIX command interpreter.\n\n", 33);
	write(1, "Execute only basic command.\n", 28);
	write(1, "No pipes, redirections or any ", 30);
	write(1, "other advanced features works.\n", 31);
}

int	main(int argc, char **argv, char **envp)
{
	if (argc == 2 && my_strcmp(argv[1], "-h") == 0) {
		display_usage();
	}
	else if (argc > 1) {
		write(2, "mysh: invalid usage: retry with -h\n", 35);
		return (84);
	}
	else {
		launch_minishell(envp);
	}
	return (0);
}
