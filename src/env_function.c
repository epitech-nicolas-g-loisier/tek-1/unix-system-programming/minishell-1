/*
** EPITECH PROJECT, 2018
** Minishell1
** File description:
** env function
*/

#include <unistd.h>
#include <stdlib.h>
#include "my.h"
#include "minishell.h"

int	env_function(char **command, char **env)
{
	int idx = 0;

	if (command[1] != NULL) {
		write(2, "Error: env non require argument\n", 32);
		return (84);
	}
	else {
		while (env[idx]) {
			write(1, env[idx], my_strlen(env[idx]));
			write(1, "\n", 1);
			idx++;
		}
	}
	return (0);
}
