/*
** EPITECH PROJECT, 2018
** Minishell 1
** File description:
** Minishell 1
*/

#ifndef MINISHELL_H_
#define MINISHELL_H_

/* cd_function */
int	print_cd_error(char*);
char	*go_to_home(char**, char*);
int	cd_function(char**, char**);

/* env_function */
int	env_function(char**, char**);

/* exit_function.c */
int	exit_function(char*);

/* get_command.c */
int	count_arg(char*);
char	*get_arg(char*, int);
char	**command_parser(char*);

/* get_path.c */
char	*get_path_line(char**);
char	*get_path(char*, int);
int	count_path(char*);
char	**path_parser(char*);

/* main.c */
int	spe_env_function(char**, char**, int);
char	**choose_command(char**, char**);
void	launch_minishell(char**);

/* other_function */
int	launch_binary(char**, char**);
int	find_prog(char**, char**);
int	other_function(char**, char**);

/* setenv_function.c */
char	*get_name(char*, char*);
int	check_name(char*, char*);
char	**get_new_env(int, char**, char*);
char	**setenv_function(char**, char**);

/* unsetenv_function.c */
int	find_name(char*, char*);
char	**delete_name(int, char**);
char	**unsetenv_function(char**, char**);

#endif /* MINISHELL_H_ */
