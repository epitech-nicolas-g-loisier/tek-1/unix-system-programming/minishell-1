##
## EPITECH PROJECT, 2017
## Minishell 1
## File description:
## Makefile
##

SRC_DIR	=	$(realpath src)

CFLAGS	=	-Wall -Wextra -Iinclude/ -L lib/ -lmy

SRC	=	$(SRC_DIR)/main.c	\
		$(SRC_DIR)/get_command.c	\
		$(SRC_DIR)/get_path.c	\
		$(SRC_DIR)/get_next_line.c	\
		$(SRC_DIR)/cd_function.c	\
		$(SRC_DIR)/env_function.c	\
		$(SRC_DIR)/exit_function.c	\
		$(SRC_DIR)/setenv_function.c	\
		$(SRC_DIR)/unsetenv_function.c	\
		$(SRC_DIR)/other_function.c

OBJ	=	$(SRC:.c=.o)

NAME	=	mysh

all:            $(NAME)

$(NAME):	$(OBJ)
		make -C lib/my/
		gcc -o $(NAME) $(OBJ) $(CFLAGS)

clean:
		rm -f $(OBJ)
		make clean -C lib/my/

fclean:		clean
		make fclean -C lib/my/
		rm -f $(NAME)

re:		fclean all
