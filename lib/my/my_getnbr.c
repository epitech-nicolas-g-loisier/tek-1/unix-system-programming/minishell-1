/*
** EPITECH PROJECT, 2018
** My Radar
** File description:
** get positive number
*/

#include <stdio.h>

int	my_getnbr(char *str)
{
	int	nb = 0;
	int	count = 0;
	int	neg = 1;

	if (str == NULL) {
		return 0;
	}
	else if (str[0] == '-') {
		neg = -1;
		count++;
	}
	while (str[count] >= '0' && str[count] <= '9'){
		nb = nb * 10;
		nb += str[count] - '0';
		count++;
	}
	nb = nb * neg;
	return (nb);
}
